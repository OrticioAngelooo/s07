/*Learning Objectives:
	- create code that will execute only when a given condition has been made
Topics:
	- Assignment Operators
	- Conditional Statements
	- Ternary Operators
*/
// Lesson Proper
/*Conditional Statements
	- a conditional statement is one of the key features of a programming language

	Ex.
		is the container full or not?
		is the temperature higher than or equal to 40 degrees celcius?
		does the title contain an ampersand(&) character?
		There are three types of conditional statements:
    - if-else statements 
    - switch statements
    - try-catch-finally statements
Operators
    - allows programming languages to execute operations or evaluations
    Assignment Operators
        - assign a value to a variable
/
/
// Basic Assignment Operator (=)
    // it allows us to assign a value to a variable
    let variable = "initial value";
// Mathematical Operators (+, -,, /, %)
    // Whenever you have used a mathematical operator, a value is returned, it is only up to us if we save that returned value. 
    // Addition, subtraction, multiplication, division assignment operators allows us to assign the result of the operation to the value of the left operand. It allows us to save the result of a mathematical operation to the left operand.
*/

    	let num1 = 5;
    	let num2 = 10;
    	let num3 = 4;
    	let num4 = 40;

/*
        Addition Assignment Operator(+=):
         Left Operand
             - is the variable or value of the left side of the opeartor
         Right Operand
             - is the variable or value of the right side of the operator
         ex. sum = num 1 + num 4 > re-assigned the value of sum with the result of num1 + num4
    */
    	let sum = num1 + num4;

    	num1 += num4;
    	console.log(num1);

    	num2 += num3;
	console.log(num2);
	num1 += 55;
	console.log(num1);
	console.log(num4);

	let string1 = "Boston";
	let string2 = "Celtics";

	string1 += string2;
	console.log(string1);// BostonCeltics - result in concatenation
	console.log(string2);//Celtics

	//subtraction assignement operator(-=)
	num1 -= num2;
	console.log("Result of subtraction assignment operator: " + num1);

	//multiplication assignement operator(*=)
	num2 *= num3;
	console.log("Result of multiplication assignment operator: " + num2);

	//division assignement operator(/=)
	num4 /= num3;
	console.log("Result of division assignment operator: " + num4);

	//Artihmetic Operators

	let x = 1397;
	let y = 7831;

	let sum1 = x + y;
	console.log("Result of addition assignment operator: " + sum);

	let difference = y - x;
	console.log("Result of subtraction assignment operator: " + difference);

	let product = x * y;
	console.log("Result of multiplication assignment operator: " + product);

	let quotient = y / x;
	console.log("Result of division assignment operator: " + quotient);

	let remainder = y % x;
	console.log("Result of modulo assignment operator: " + remainder);
// Multiple Operators and Parenthesis
/*
	- when multiple operators are applied in a single parenthesis/statement, it follows the PEMDAS (Paranthesis, Exponents, Multiplication, Division, Addition, Subtraction)
	- the operations were done in the following order:
		expression = 1 + 2 - 3 * 4 / 5;
		1. 3 * 4 = 12
		2. 12 / 5 = 2.4
		3. 1 + 2 = 3
		4. 3 - 2.4 = 0.6
*/
let mdas = 1 + 2 - 3 * 4 / 5;
console.log("Result of multiple operations: " + mdas);

// order of operation can be changed by adding parentheses to the logic
let pemdas = 1 + (2 - 3) * (4 / 5);
/*
	- by adding parentheses "()", the order of operation changed priority to operations inside the parentheses first then follow mdas rule
	- the operations were done in the following order:
		1. 4 / 5 = 0.8
		2. 2 - 3 = = -1
		3. -1 * 0.8 = -0.8
		4. 1 + -0.8 = 0.19
*/
console.log("Result of PEMDAS operation: " + pemdas);

pemdas = (1 + (2 - 3)) * (4 / 5);
console.log("Result of PEMDAS 2nd Example: " + pemdas);

// Increment and Decrement
	// Increment and Decrement is adding or subtracting 1 from the variable and re-assigning the new value to the variable where the increment or decrement was used
	// 2 kinds of incrementation: Pre-fix and Post-fix

	let z = 1;

	// Pre-fix Incrementation
	++z;
	console.log("Pre-fix increment: " + z);

	// Post-fix Incrementation
	z++;
	console.log("Post-fix Increment: " + z); // 3 - The value of z was added with 1
	console.log(z++); // 3 - with post-incrementation the previous value of the variable is returned first before the actual incrementation
	console.log(z); // 4 - new value is now returned

	// Pre-fix vs Post-fix
	console.log(z);
	console.log(z++); // 4 previous value was returned first
	console.log(z); // 5 new value is returned

	console.log(++z);

	// Pre-fix and Post-fix Decrementation
	console.log(--z);
	console.log(z--);
	console.log(z);

// Comparison Operators
	/* Comparison Operators 
		- are used to compare the values of the left and right operands
		- comparison operators return boolean
			- Equality or Loose equality operator (==)
			- Strict equality operator (===)
	*/
	console.log(1 == 1);

	let isSame = 55 == 55;
	console.log(isSame);

	console.log(1 == '1');
	console.log(0 == false);
	console.log(1 == true);
	console.log("false" == false);
	console.log(true == "true");

	

	console.log(1 == '1'); //true - loose equality operator priority is the sameness of the value because with loose equality operator, forced coercion is done before comparison - JS forcibly changes the data type to the operands
	console.log(0 == false); //true - with forced coercion, false was converted into a number results into NaN so therefore, 1 is not equal to NaN
	console.log(1 == true); //true
	console.log("false" == false); // false
	console.log(true == "true") // false
	/*
		with loose comparison operator (==), values are compared and types if operands do not have the same types,it will be forced coerced/type coerce before comparison value

		if either the operand is a number of boolean, the operands are converted into numbers.
	*/
	console.log(true == "1"); // true - true is coerced into a number = 1, "1" converted into a number = 1 - 1 equals 1

	// Strict Equality (===)
	console.log(true === "1"); //false

	// check both value and type
	console.log(1 === '1'); // false - operands have the same values but different types
	console.log("BTS" === "BTS"); // true - same value and same typed
	console.log("Marie" === "marie"); // false - left operand is capitalized and right operand is small caps

	console.log('1' != 1);

	console.log("James" != "John");
	console.log(1 != true);//false

	/*
		false - with type conversion: true was converted to 1 is equal to 1
		it is NOT inequal
	*/

	// Strict Inequality (!==) - checkes whether the two operands have different values and will also check if they have different types
	console.log("5" !== 5); // true - operands are inequal because they have different types
	console.log(5 !== 5); // false - operands are equal they have the same value and they have the same type

	let name1 = "Jin";
	let name2 = "Jimin";
	let name3 = "Jungkook";
	let name4 = "V";

	let number1 = 50;
	let number2 = 60;
	let numString1 = "50";
	let numString2 = "60";

	console.log(numString1 == number1);
	console.log(numString1 === number1);
	console.log(numString1 != number1);
	console.log(name4 !== "num3");
	console.log(name3 == "Jungkook");
	console.log(name1 == "Jin");

	/* Relational Comparison Operators
	 a comparison operator compares its operand and returns a boolean value based on whether the comparison is true
*/

let a = 500;
let b = 700;
let c = 8000;
let numString3 = "5500";

	// Greater than (>)
	console.log(a > b);
	console.log(c > y);

	// Less than (<)
	console.log(c < a); // false
	console.log(b < b); // false
	console.log(a < 1000); // true
	console.log(numString3 < 1000); // false - forced coercion to change the string into number
	console.log(numString3 < 6000); // true
	console.log(numString3 < "Jose"); // true - "5500" < Jose - that is erratic (unpredictable)

	// Greater than or Equal to (>=)
	console.log(c >= 10000); // false
	//console.log(b >= x); // false
	console.log(b >= a); // true

	// Less than or Equal to (<=)
	console.log(a <= b); //true
	console.log(c <= a); //false

/* Logical Operators
	AND operator (&&)
		- both operands on the left and right or all operands must be true otherwise false

		T && T = true
		T && F = false
		F && T = false
		F && F = false
*/

	let isAdmin = false;
	let isRegistered = true;
	let isLegalAge = true;

	let authorization1 = isAdmin && isRegistered;
	console.log(authorization1); //false

	let authorization2 = isLegalAge && isRegistered;
	console.log(authorization2); //true

	let requiredLevel = 95;
	let requiredAge = 18;

	let authorization3 = isRegistered && requiredLevel === 25;
	console.log(authorization3); //false

	let authorization4 = isRegistered && isLegalAge && requiredLevel === 95;
	console.log(authorization4);

	let userName1 = "gamer2001";
	let userName2 = "shadow1991"; 
	let userAge = 15;
	let userAge2 = 30;

	let registration1 = userName1.length > 8 && userAge >= requiredAge;
	// .length is a property of string which determines the number of characters in the string
	console.log(registration1); // false - userAge does not meet the required age = 18

	let registration2 = userName2.length > 8 && userAge2 >= requiredAge;
	console.log(registration2); // true

	/* OR Operator (|| - double pipe)
		returns true if at least one of the operands are true.
		T || T = true
		T || F = true
		F || T = true
		F || F = false
	*/
	let userLevel = 100;
	let userLevel2 = 65;

	let guildRequirment1 = isRegistered && userLevel >= requiredLevel && userAge >= requiredAge;
	console.log(guildRequirment1); // false

	let guildRequirment2 = isRegistered || userLevel >= requiredLevel || userAge >= requiredAge;
	console.log(guildRequirment2); // true

	let guildRequirment3 = userLevel >= requiredLevel ||  userAge >= requiredAge;
	console.log(guildRequirment3); //true

	let guildAdmin = isAdmin || userLevel2 >= requiredLevel;
	console.log(guildAdmin); //false

	// Not Operator - it turns a boolean into the opposite value

	let guildAdmin2 = !isAdmin || userLevel2 >= requiredLevel;
	console.log(guildAdmin2); // true

	console.log(!isRegistered); // false

	// Conditional
		// if-else statements - will run a block of code if the condition specified is true or results to true

		let userName3 = "Crusader_1993";
		let userLevel3 = 25;
		let userAge3 = 20;

		// if(true){
		// 	alert("We just run an if condition!")
		// };

		if(userName3.length > 10){
			console.log("Welcome to Game Online!");
		};

		if(userLevel3 >= requiredLevel){
			console.log("You are qualified to join the guild.");
		};

		// else statement will run if the condition given is false or results to false

		if(userName3.length > 10 && userLevel3 <= 25 && userAge3 >= requiredAge){
			console.log("Thank you for joining the Noobies Guild.");
		} else {
			console.log("You are too strong to be a noob. :(");
		};

		if(userName3.length >= 10 && userLevel3 <= 25 && userAge >= requiredAge){
			console.log("Thank you for joining the noobies guild.");
	} else if(userLevel3 > 25){
			console.log("You are too strong to be a noob.");
	} else if(userAge3 < requiredAge){
			console.log("You are too young to join the guild.");
	} else if(userName3.length < 10){
		console.log("Username too short.")
	};

	// if-else in function
	function addNum(num1, num2){
		// check if the numbers being passed argumetn are number types
		// typeof keyword returns a string which tells the type of data that follow it
		if(typeof num1 === 'number' && typeof num2 === 'number'){
			console.log("run only if both arguments passed are number types.");
			console.log(num1 + num2);
		} else {
			console.log("One or both of the arguments are not numbers");
		};
	};

	addNum(5,"2");

	function login(username, password){
		// how can we check if the argument passed are strings?
		if(typeof username === 'string' &&  typeof password === 'string'){
			console.log("Both arguments are strings.");
			/*Nested if-else
				will run if the parent if statement is able to agree to accomplish its condition

				Mini-Activity
					- add another condition to our nested if statement:
						check if the passwoed is at least 8 characters long
					- add an else statement which will run if both conditions are not met:
						- show an alert which says "Credentials too short."

					Stretch Goals:
						- add an else if statement that if the username is less than 8 characters
							- show an alert "username is too short."
						- add an else if statement that if the password is less than 8 characters 
							- show an alert "passwoed is too short."
			*/ 
		};
	};
		//mini-activity
	function login(user, pass){
           if(typeof user ==='string'&& typeof pass === 'string'){
            console.log("Both arguments are strings");
        }
        if(user.length>=8 && pass.length>=8)
           {
                console.log("Logging in!")
           }
            else if (pass.length <8)
           {
                console.log(alert("The password is too short"))
           }
            else if (user.length<8)
           {
                console.log(alert("The username is too short"))
           }
            else{
                console.log(alert("Details are too short"));
           }
    }
    login('Shubaxx','followMeOnFB');